//alert("hello world");

//arrays 
/*
-an array in programming si simply list of data.

*/



let studentNumberA = '2023-1923';
let studentNumberB = '2024-1923';
let studentNumberC = '2025-1923';
let studentNumberD = '2026-1923';
let studentNumberE = '2027-1923';

// [] array literals
//they are usually store numerous amount of data to manipu;ate in order to perform a  number of task
//arrays provide number of function t
//the main diff of arry and object is that  contain information in a form of list, unlike objcets that uses properties
// with array 
let studentNumbers = [
					'2023-1923',
					'2024-1923',
					'2025-1923',
					'2026-1923',
					'2027-1923',
					];

let computeBrands = [
	'acer',
	'alienware',
	'asus'
	]

 console.log(computeBrands);

//creating an arrays with values from variable


 let city1 = "tokyo";
 let city2 = "manila";
 let city3 = "jakarta";

let cities = [city1, city2, city3, 'bangkok']
console.log(cities);


//length property 
// the .length property allows us to get and set the total number of itmes in an array

console.log (cities[2].length);


let blankArr =[2];
console.log*(blankArr.length);

// .length properties can used with string some array methods properties cean be used with strings
let fullname ="jake lexter 123";
console.log(fullname.length);


cities.length =cities.length-1;
console.log(cities.length);
console.log(cities);

//using decrementation
cities.length--;
console.log(cities)


//adding an item in an array 
//if you can shorted the array by setting the lengthh by adding a number into the lenght of property

let theBeatles = [
	"john",
	"paul",
	"ringo",
	"george",
	];
theBeatles.length++;
console.log(theBeatles);

//read from arrays
/*
	we can acccess array elements through the use of array indexes

	in javascript the first number is associated with nmber 0

	array indexes acuall refers to an location in the devices  hot information stored
	
	synatx:
	arrayName[index]


*/


console.log(computeBrands[1])

let lakersLegend =[
	'kobe',
	'shaq',
	'lebron',
	'magic',
	'kareem',
	]
//shaq
console.log(lakersLegend[1])
//magic
console.log(lakersLegend[3])
//store arry item in variable
let currentLaker = lakersLegend[2];
console.log(currentLaker);

//we can reassign array values using itme indices
console.log("array before assignment");
console.log(lakersLegend);
lakersLegend[2] ="paul gasul";
console.log("after assignment")
console.log(lakersLegend);

//accesiong the last element of an array 
let bullsLegend = ["jordam", "pippen", "rose", "kukoc"]
let lastelement = bullsLegend.length-1;
console.log(bullsLegend[lastelement])


//add items into array
//using index
let newArr = [];
console.log(newArr);
newArr[0] = "kuku"
console.log(newArr);
newArr[2] = "gabbi"
console.log(newArr);
newArr[4] = "eyou"
console.log(newArr);
// add always to the last
newArr[newArr.length] = "raven"
console.log(newArr);


//loopomg over an array
for (let index =0; index < newArr.length; index++){
	console.log(newArr[index])
}


let numArr  = [5, 12, 30, 46, 40];

for (let index = 0; index <numArr.length; index++){
	if(numArr[index] % 5){
		console.log(numArr[index]+ "divisible by 5")
}
	else{
		console.log(numArr[index]+ " is not divisible by 5")
	}
	
}




//multi dimensional arrays

/*
	are usefull for storing complex data structures
*/








